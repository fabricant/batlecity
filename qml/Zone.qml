import QtQuick 2.0

Rectangle {
    width: 48
    height: 48
    objectName: "zone"
    property int row: 0
    property int column: 0
    color: "transparent"
    onColumnChanged: {
        x = width * column
    }

    onRowChanged:
    {
        y = height * row
    }
}
