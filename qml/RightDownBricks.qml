import QtQuick 2.0

Zone {
    LeftBrick {
        x: width * 2
        y:parent.height - height
    }

    RightBrick {
        x:parent.width - width
        y:parent.height - height
    }

    LeftBrick {
        x: width * 2
        y:height * 2
    }

    RightBrick {
        x:parent.width - width
        y:height * 2
    }
}
