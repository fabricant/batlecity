import QtQuick 2.0
Item {
    width:42
    height: 42
    objectName: "bullet"
//    color: "green"
    Image {
        source: "sprites/bullet.png"
        smooth: false
        width: 3
        height: 4
        anchors.centerIn: parent

        Behavior on y {
            NumberAnimation {
                easing {
                    type: Easing.Linear
                }
            }
        }

        Behavior on x {
            NumberAnimation {
                easing {
                    type: Easing.Linear
                }
            }
        }



        //    onDestroyed: {}
    }

    function next() {
        switch (rotation) {
        case 0:
            y -= 10;
            break
        case 90:
            x += 10;
            break
        case 180:
            y += 10;
            break
        case 270:
            x -= 10
        }
    }

    function deleteMe() {
        destroy()
    }
}
