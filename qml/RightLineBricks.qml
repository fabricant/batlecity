import QtQuick 2.0

Zone {
    LeftBrick {
        x: width * 2
    }

    RightBrick {
        x: parent.width - width
    }

    LeftBrick {
        x: width * 2
        y:height
    }

    RightBrick {
        x: parent.width - width
        y: height
    }

    LeftBrick {
        x: width * 2
        y:height * 2
    }

    RightBrick {
        x: parent.width - width
        y: height * 2
    }

    LeftBrick {
        x: width * 2
        y:height * 3
    }

    RightBrick {
        x: parent.width - width
        y: height * 3
    }

}
