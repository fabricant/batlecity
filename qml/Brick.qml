import QtQuick 2.0

Image {
    source: "sprites/brick.png"
    width: 32
    height: 16
    smooth: false
    objectName: "brick"

    function deleteMe() {
        destroy()
    }
}
