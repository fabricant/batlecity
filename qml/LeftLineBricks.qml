import QtQuick 2.0

Zone {
    LeftBrick {}
    RightBrick {
        x: width
    }

    LeftBrick {
        y:height
    }

    RightBrick {
        x: width
        y: height
    }

    LeftBrick {
        y:height * 2
    }

    RightBrick {
        x: width
        y: height * 2
    }

    LeftBrick {
        y:height * 3
    }

    RightBrick {
        x: width
        y: height * 3
    }

}
