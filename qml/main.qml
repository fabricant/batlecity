import QtQuick 2.8
import QtQuick.Window 2.2

Window {
    id: mainWindow
    objectName: "mainWindow"
    visible: true
    width: 624
    height: 624
    title: qsTr("Hello World")

    Level1 {

        id:rect
        anchors.fill: parent
        color:"black"
        focus: true
        objectName: "map"
        Tank {
            id :mainTank
            objectName :"mainTank"
            source: "sprites/main_tank.png"
            y:parent.height - height
            x:1
        }

        Tank {
            id: enemyTank1
            objectName: "enemyTank"
            x: 1
            y: 0
            rotation: 180

            source: "sprites/enemy_tank.png"
        }
    }
}


