import QtQuick 2.0

Zone {
    LeftBrick { }

    RightBrick {
        x: width
    }

    LeftBrick {
        x: width * 2
    }

    RightBrick {
        x: parent.width - width
    }

    //-------------------------------

    LeftBrick {
        y: height
    }

    RightBrick {
        y: height
        x: width
    }

    LeftBrick {
        x: width * 2
        y: height
    }

    RightBrick {
        x: parent.width - width
        y: height
    }
}
