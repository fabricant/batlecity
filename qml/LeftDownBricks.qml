import QtQuick 2.0

Zone {
    LeftBrick {
        y:parent.height - height
    }

    RightBrick {
        x:width
        y:parent.height - height
    }

    LeftBrick {
        y:height * 2
    }

    RightBrick {
        x:width
        y:height * 2
    }
}
