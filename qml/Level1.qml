import QtQuick 2.0

Rectangle {
    id: rectangle
    visible: true
    width: 624
    height: 624
    color: "black"
//row 12 ----------------------------------
    Zone {
        id:x0y12
        column: 0
        row: 12
    }

    Zone {
        id:x1y12
        column: 1
        row: 12
    }

    Zone {
        id:x2y12
        column: 2
        row: 12
    }

    Zone {
        id:x3y12
        column: 3
        row: 12
    }

    Zone {
        id:x4y12
        column: 4
        row: 12
    }

    RightLineBricks {
        id:x5y12
        column: 5
        row: 12
    }


    Statue {
        id:x6y12
        column: 6
        row: 12
    }

    LeftLineBricks {
        id:x7y12
        column: 7
        row: 12
    }

    Zone {
        id:x8y12
        column: 8
        row: 12
    }

    Zone {
        id:x9y12
        column: 9
        row: 12
    }

    Zone {
        id:x10y12
        column: 10
        row: 12
    }

    Zone {
        id:x11y12
        column: 11
        row: 12
    }

    Zone {
        id:x12y12
        column: 12
        row: 12
    }

    //row 11 ----------------------------------

    Zone {
        id:x0y11
        column: 0
        row: 11
    }

    SquareBricks {
        id:x1y11
        column: 1
        row: 11
    }

    Zone {
        id:x2y11
        column: 2
        row: 11
    }

    SquareBricks {
        id:x3y11
        column: 3
        row: 11
    }

    Zone {
        id:x4y11
        column: 4
        row: 11
    }

    RightDownBricks {
        id:x5y11
        column: 5
        row: 11
    }

    DownLineBricks {
        id:x6y11
        column: 6
        row: 11
    }

    LeftDownBricks {
        id:x7y11
        column: 7
        row: 11
    }

    Zone {
        id:x8y11
        column: 8
        row: 11
    }

    SquareBricks {
        id:x9y11
        column: 9
        row: 11
    }

    Zone {
        id:x10y11
        column: 10
        row: 11
    }

    SquareBricks {
        id:x11y11
        column: 11
        row: 11
    }

    Zone {
        id:x12y11
        column: 12
        row: 11
    }

    //row 10 ----------------------------------

    Zone {
        id:x0y10
        column: 0
        row: 10
    }

    SquareBricks {
        id:x1y10
        column: 1
        row: 10
    }

    Zone {
        id:x2y10
        column: 2
        row: 10
    }

    SquareBricks {
        id:x3y10
        column: 3
        row: 10
    }

    Zone {
        id:x4y10
        column: 4
        row: 10
    }

    UpLineBricks {
        id:x5y10
        column: 5
        row: 10
    }

    Zone {
        id:x6y10
        column: 6
        row: 10
    }

    UpLineBricks {
        id:x7y10
        column: 7
        row: 10
    }

    Zone {
        id:x8y10
        column: 8
        row: 10
    }

    SquareBricks {
        id:x9y10
        column: 9
        row: 10
    }

    Zone {
        id:x10y10
        column: 10
        row: 10
    }

    SquareBricks {
        id:x11y10
        column: 11
        row: 10
    }

    Zone {
        id:x12y10
        column: 12
        row: 10
    }

    //row 9 ----------------------------------

    Zone {
        id:x0y9
        column: 0
        row: 9
    }

    SquareBricks {
        id:x1y9
        column: 1
        row: 9
    }

    Zone {
        id:x2y9
        column: 2
        row: 9
    }

    SquareBricks {
        id:x3y9
        column: 3
        row: 9
    }

    Zone {
        id:x4y9
        column: 4
        row: 9
    }

    SquareBricks {
        id:x5y9
        column: 5
        row: 9
    }

    Zone {
        id:x6y9
        column: 6
        row: 9
    }

    SquareBricks {
        id:x7y9
        column: 7
        row: 9
    }

    Zone {
        id:x8y9
        column: 8
        row: 9
    }

    SquareBricks {
        id:x9y9
        column: 9
        row: 9
    }

    Zone {
        id:x10y9
        column: 10
        row: 9
    }

    SquareBricks {
        id:x11y9
        column: 11
        row: 9
    }

    Zone {
        id:x12y9
        column: 12
        row: 9
    }


    //row 8 ----------------------------------

    Zone {
        id:x0y8
        column: 0
        row: 8
    }

    DownLineBricks {
        id:x1y8
        column: 1
        row: 8
    }

    Zone {
        id:x2y8
        column: 2
        row: 8
    }

    DownLineBricks {
        id:x3y8
        column: 3
        row: 8
    }

    Zone {
        id:x4y8
        column: 4
        row: 8
    }

    SquareBricks {
        id:x5y8
        column: 5
        row: 8
    }

    SquareBricks {
        id:x6y8
        column: 6
        row: 8
    }

    SquareBricks {
        id:x7y8
        column: 7
        row: 8
    }

    Zone {
        id:x8y8
        column: 8
        row: 8
    }

    DownLineBricks {
        id:x9y8
        column: 9
        row: 8
    }

    Zone {
        id:x10y8
        column: 10
        row: 8
    }

    DownLineBricks {
        id:x11y8
        column: 11
        row: 8
    }

    Zone {
        id:x12y8
        column: 12
        row: 8
    }


    //row 7 ----------------------------------

    UpLineConcrete {
        id:x0y7
        column: 0
        row: 7
    }

    Zone {
        id:x1y7
        column: 1
        row: 7
    }

    UpLineBricks {
        id:x2y7
        column: 2
        row: 7
    }

    UpLineBricks {
        id:x3y7
        column: 3
        row: 7
    }

    Zone {
        id:x4y7
        column: 4
        row: 7
    }

    DownLineBricks {
        id:x5y7
        column: 5
        row: 7
    }

    Zone {
        id:x6y7
        column: 6
        row: 7
    }

    DownLineBricks {
        id:x7y7
        column: 7
        row: 7
    }

    Zone {
        id:x8y7
        column: 8
        row: 7
    }

    UpLineBricks {
        id:x9y7
        column: 9
        row: 7
    }

    UpLineBricks {
        id:x10y7
        column: 10
        row: 7
    }

    Zone {
        id:x11y7
        column: 11
        row: 7
    }

    UpLineConcrete {
        id:x12y7
        column: 12
        row: 7
    }

    //row 6 ----------------------------------

    DownLineBricks {
        id:x0y6
        column: 0
        row: 6
    }

    Zone {
        id:x1y6
        column: 1
        row: 6
    }

    DownLineBricks {
        id:x2y6
        column: 2
        row: 6
    }

    DownLineBricks {
        id:x3y6
        column: 3
        row: 6
    }

    Zone {
        id:x4y6
        column: 4
        row: 6
    }

    UpLineBricks {
        id:x5y6
        column: 5
        row: 6
    }

    Zone {
        id:x6y6
        column: 6
        row: 6
    }

    UpLineBricks {
        id:x7y6
        column: 7
        row: 6
    }

    Zone {
        id:x8y6
        column: 8
        row: 6
    }

    DownLineBricks {
        id:x9y6
        column: 9
        row: 6
    }

    DownLineBricks {
        id:x10y6
        column: 10
        row: 6
    }

    Zone {
        id:x11y6
        column: 11
        row: 6
    }

    DownLineBricks {
        id:x12y6
        column: 12
        row: 6
    }


    //row 5 ----------------------------------

    Zone {
        id:x0y5
        column: 0
        row: 5
    }

    UpLineBricks {
        id:x1y5
        column: 1
        row: 5
    }

    Zone {
        id:x2y5
        column: 2
        row: 5
    }

    UpLineBricks {
        id:x3y5
        column: 3
        row: 5
    }

    Zone {
        id:x4y5
        column: 4
        row: 5
    }

    DownLineBricks {
        id:x5y5
        column: 5
        row: 5
    }

    Zone {
        id:x6y5
        column: 6
        row: 5
    }

    DownLineBricks {
        id:x7y5
        column: 7
        row: 5
    }

    Zone {
        id:x8y5
        column: 8
        row: 5
    }

    UpLineBricks {
        id:x9y5
        column: 9
        row: 5
    }

    Zone {
        id:x10y5
        column: 10
        row: 5
    }

    UpLineBricks {
        id:x11y5
        column: 11
        row: 5
    }

    Zone {
        id:x12y5
        column: 12
        row: 5
    }

    //row 4 ----------------------------------

    Zone {
        id:x0y4
        column: 0
        row: 4
    }

    SquareBricks {
        id:x1y4
        column: 1
        row: 4
    }

    Zone {
        id:x2y4
        column: 2
        row: 4
    }

    SquareBricks {
        id:x3y4
        column: 3
        row: 4
    }

    Zone {
        id:x4y4
        column: 4
        row: 4
    }

    UpLineBricks {
        id:x5y4
        column: 5
        row: 4
    }

    Zone {
        id:x6y4
        column: 6
        row: 4
    }

    UpLineBricks {
        id:x7y4
        column: 7
        row: 4
    }

    Zone {
        id:x8y4
        column: 8
        row: 4
    }

    SquareBricks {
        id:x9y4
        column: 9
        row: 4
    }

    Zone {
        id:x10y4
        column: 10
        row: 4
    }

    SquareBricks {
        id:x11y4
        column: 11
        row: 4
    }

    Zone {
        id:x12y4
        column: 12
        row: 4
    }

    //row 3 ----------------------------------

    Zone {
        id:x0y3
        column: 0
        row: 3
    }

    SquareBricks {
        id:x1y3
        column: 1
        row: 3
    }

    Zone {
        id:x2y3
        column: 2
        row: 3
    }

    SquareBricks {
        id:x3y3
        column: 3
        row: 3
    }

    Zone {
        id:x4y3
        column: 4
        row: 3
    }

    SquareBricks {
        id:x5y3
        column: 5
        row: 3
    }

    SquareConcrete {
        id:x6y3
        column: 6
        row: 3
    }

    SquareBricks {
        id:x7y3
        column: 7
        row: 3
    }

    Zone {
        id:x8y3
        column: 8
        row: 3
    }

    SquareBricks {
        id:x9y3
        column: 9
        row: 3
    }

    Zone {
        id:x10y3
        column: 10
        row: 3
    }

    SquareBricks {
        id:x11y3
        column: 11
        row: 3
    }

    Zone {
        id:x12y3
        column: 12
        row: 3
    }

    //row 2 ----------------------------------

    Zone {
        id:x0y2
        column: 0
        row: 2
    }

    SquareBricks {
        id:x1y2
        column: 1
        row: 2
    }

    Zone {
        id:x2y2
        column: 2
        row: 2
    }

    SquareBricks {
        id:x3y2
        column: 3
        row: 2
    }

    Zone {
        id:x4y2
        column: 4
        row: 2
    }

    SquareBricks {
        id:x5y2
        column: 5
        row: 2
    }

    Zone {
        id:x6y2
        column: 6
        row: 2
    }

    SquareBricks {
        id:x7y2
        column: 7
        row: 2
    }

    Zone {
        id:x8y2
        column: 8
        row: 2
    }

    SquareBricks {
        id:x9y2
        column: 9
        row: 2
    }

    Zone {
        id:x10y2
        column: 10
        row: 2
    }

    SquareBricks {
        id:x11y2
        column: 11
        row: 2
    }

    Zone {
        id:x12y2
        column: 12
        row: 2
    }

    //row 1 ----------------------------------

    Zone {
        id:x0y1
        column: 0
        row: 1
    }

    SquareBricks {
        id:x1y1
        column: 1
        row: 1
    }

    Zone {
        id:x2y1
        column: 2
        row: 1
    }

    SquareBricks {
        id:x3y1
        column: 3
        row: 1
    }

    Zone {
        id:x4y1
        column: 4
        row: 1
    }

    SquareBricks {
        id:x5y1
        column: 5
        row: 1
    }

    Zone {
        id:x6y1
        column: 6
        row: 1
    }

    SquareBricks {
        id:x7y1
        column: 7
        row: 1
    }

    Zone {
        id:x8y1
        column: 8
        row: 1
    }

    SquareBricks {
        id:x9y1
        column: 9
        row: 1
    }

    Zone {
        id:x10y1
        column: 10
        row: 1
    }

    SquareBricks {
        id:x11y1
        column: 11
        row: 1
    }

    Zone {
        id:x12y1
        column: 12
        row: 1
    }

    //row 0 ----------------------------------

    Zone {
        id:x0y0
        column: 0
        row: 0
    }

    Zone {
        id:x1y0
        column: 1
        row: 0
    }

    Zone {
        id:x2y0
        column: 2
        row: 0
    }

    Zone {
        id:x3y0
        column: 3
        row: 0
    }

    Zone {
        id:x4y0
        column: 4
        row: 0
    }

    Zone {
        id:x5y0
        column: 5
        row: 0
    }

    Zone {
        id:x6y0
        column: 6
        row: 0
    }

    Zone {
        id:x7y0
        column: 7
        row: 0
    }

    Zone {
        id:x8y0
        column: 8
        row: 0
    }

    Zone {
        id:x9y0
        column: 9
        row: 0
    }

    Zone {
        id:x10y0
        column: 10
        row: 0
    }

    Zone {
        id:x11y0
        column: 11
        row: 0
    }

    Zone {
        id:x12y0
        column: 12
        row: 0
    }
}


