import QtQuick 2.0

Zone {
    LeftBrick {
        y: parent.height - height
    }

    RightBrick {
        y: parent.height - height
        x: width
    }

    LeftBrick {
        x: width * 2
        y: parent.height - height
    }

    RightBrick {
        x: parent.width - width
        y: parent.height - height
    }

    //-------------------------------

    LeftBrick {
        y: height * 2
    }

    RightBrick {
        y: height * 2
        x: width
    }

    LeftBrick {
        x: width * 2
        y: height * 2
    }

    RightBrick {
        x: parent.width - width
        y: height * 2
    }
}
