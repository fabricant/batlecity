import QtQuick 2.0

Zone {
    Image {
        objectName: "concrete"
        source: "sprites/concrete_line.png"
        width: parent.width
        height: parent.height / 2
        smooth: false
    }
}
