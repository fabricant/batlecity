import QtQuick 2.0

Image {
    objectName: "brick"
    source: "sprites/left_brick.png"
    width: 12
    height: 12
    smooth: false

    function deleteMe() {
        destroy()
    }
}
