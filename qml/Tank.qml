import QtQuick 2.0

Image {
    smooth: false
    width: 42
    height: 42
    fillMode:Image.PreserveAspectFit
    Behavior on y {
        NumberAnimation {
            easing {
                type: Easing.Linear
            }
        }
    }

    Behavior on x {
        NumberAnimation {
            easing {
                type: Easing.Linear
            }        }
    }

    function setCoordinates(newX, newY) {
        x = newX
        y = newY

    }
    function createBullet() {

        var component = Qt.createComponent("Bullet.qml");
        var object = component.createObject(parent, {x:x, y:y});
        object.rotation = rotation
    }
}
