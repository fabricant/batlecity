#ifndef ZONE_H
#define ZONE_H
#include <QQuickItem>

class Zone
{
public:
    Zone(QQuickItem *item);
    inline QQuickItem *getZone();
    inline QList<QQuickItem*>getBricks();
    bool checkColisitionWithRect(qreal x, qreal y, qreal width, qreal height);
    bool removeBricksAtPath(qreal x, qreal y, qreal width, qreal height);
protected:
    QQuickItem *zoneItem;
    bool checkCollisionsOfItems(qreal x1, qreal x2, qreal y1, qreal y2, qreal width1, qreal width2,
                                      qreal height1, qreal height2);
};

#endif // ZONE_H
