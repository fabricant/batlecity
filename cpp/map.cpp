#include "constants.h"
#include "map.h"

Map::Map(){}

void Map::setMapItem(QQuickItem *item)
{
    mapItem = item;
}

bool Map::checkCollisionForrect(qreal x, qreal y, qreal width, qreal height)
{
    if (x < 0 || y < 0 || x + width > mapItem->width() || y + height > mapItem->height())
        return false;

    Zone **zonesOfItem = new Zone*[4];
    int zonesCount = 0;
    findZonesOfItem(x, y, width, height, zonesOfItem, &zonesCount);

    for (int i = 0; i < zonesCount; ++i)
    {
        if (zonesOfItem[i]->checkColisitionWithRect(x, y, width, height))
        {
            delete [] zonesOfItem;
            return false;
        }
    }
    return true;
}


void Map::findZonesOfItem(qreal x, qreal y, qreal width, qreal height, Zone **zonesOfItem, int *zonesCount)
{
    int x1 = x / ZONE_WIDTH;
    int x2 = (x + width - 1) / ZONE_WIDTH;
    int y1 = y / ZONE_HEIGT;
    int y2 = (y + height - 1) / ZONE_HEIGT;

    zonesOfItem[0] = &(zones[y1 * ZONE_PER_LINE + x1]);


    if (x1 == x2)
    {
        if (y1 == y2) {
            *zonesCount = 1;
        }
        else
        {
            zonesOfItem[1] = &(zones[y2 * ZONE_PER_LINE + x1]);
            *zonesCount = 2;
        }
    }
    else
    {
        zonesOfItem[1] = &(zones[y1 * ZONE_PER_LINE + x2]);
        if (y1 == y2)
        {
            *zonesCount = 2;
        }
        else
        {
            zonesOfItem[2] = &(zones[y2 * ZONE_PER_LINE + x1]);
            zonesOfItem[3] = &(zones[y2 * ZONE_PER_LINE + x2]);
            *zonesCount = 4;

        }
    }

}

void Map::appendZone(Zone z)
{
    zones.append(z);
}

void Map::getPosibleNewDirectionToItem(QQuickItem *item, Direction currentDirection, Direction *buffer, unsigned int &directionsCount)
{
    directionsCount = 0;
    switch (currentDirection) {
    case Up:
    case Down:
        if (checkCollisionForrect(item->x() + SPEED, item->y(), item->width(), item->height()))
        {
            buffer[directionsCount++] = Right;
        }

        if (checkCollisionForrect(item->x() - SPEED, item->y(), item->width(), item->height()))
        {
            buffer[directionsCount++] = Left;
        }        break;
    case  Left:
    case Right:

        if (checkCollisionForrect(item->x(), item->y() + SPEED, item->width(), item->height()))
        {
            buffer[directionsCount++] = Down;
        }

        if (checkCollisionForrect(item->x(), item->y() - SPEED, item->width(), item->height()))
        {
            buffer[directionsCount++] = Up;
        }
        break;
    default:
        break;
    }
}
