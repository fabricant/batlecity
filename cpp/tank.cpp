#include "tank.h"

Tank::Tank(QQuickItem *item):tankItem(item) {}

void Tank::setCouldChangeDirection(bool ccd)
{
    couldChangeDirection = ccd;
}

bool Tank::getPreviousDirection()
{
    return couldChangeDirection;
}

QQuickItem *Tank::getTank()
{
    return tankItem;
}

void Tank::remove()
{

}
