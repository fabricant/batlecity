#ifndef KEYBOARDEVENTS_H
#define KEYBOARDEVENTS_H

#include <QObject>
#include <QQuickItem>

#include "zone.h"
#include "map.h"
#include "tank.h"


class TanksCalculation : public QObject
{
    Q_OBJECT

public:
    explicit TanksCalculation(QObject *parent = nullptr);

    void setMap(QObject *object);
    void run();


protected:
    bool eventFilter(QObject *watched, QEvent *event);
    QQuickItem *mainTank;
    QQuickItem *mapItem;
    Map map;
    QTimer *timer;
    void update();
    void enemyMove();
//    QList <QQuickItem*> bricks;
    QList <QQuickItem*> bullets;
    QList <Tank> enemyTanks;
    void getPosibleNewDirectionToItem(QQuickItem *item, Direction currentDirection, Direction *buffer, unsigned int &directionsCount);
//    inline bool checkCollisionsOfItems(QPointF point1, QPointF point2, qreal width1, qreal width2,
//                                                  qreal height1, qreal height2);
    inline bool updateTankPath(Direction drc, QQuickItem *tank);
    inline void updateBulletPath();

 Direction direction;

signals:

public slots:
};



#endif // KEYBOARDEVENTS_H
