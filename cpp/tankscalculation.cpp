#include <QEvent>
#include <QTimer>
#include <time.h>

#include "tankscalculation.h"
#include "constants.h"

TanksCalculation::TanksCalculation(QObject *parent) : QObject(parent)
{
    installEventFilter(this);
    srand(time(nullptr));
}


void TanksCalculation::setMap(QObject *object)
{
    mapItem = object->findChild<QQuickItem*>("map");
    map.setMapItem(mapItem);
    mainTank = mapItem->findChild<QQuickItem*>(MAIN_TANK);
    object->installEventFilter(this);
    QList <QQuickItem*> listOfEnemys = mapItem->findChildren<QQuickItem*>("enemyTank");

    for (int i = 0; i < listOfEnemys.count(); ++i)
    {
        Tank t(listOfEnemys[i]);
        enemyTanks.append(t);
    }

    QList <QQuickItem*> listOfZones = mapItem->findChildren<QQuickItem *>("zone");

    qSort(listOfZones.begin(), listOfZones.end(), [](QQuickItem *&item1, QQuickItem *&item2) -> bool
    {

        if (item1->y() == item2->y())
            return item1->x() < item2->x();

        return item1->y() < item2->y();

    });

    for (int i = 0; i < listOfZones.count(); ++i)
    {
        Zone z(listOfZones[i]);
        qDebug() << listOfZones[i];
        map.appendZone(z);
    }
}

void TanksCalculation::run()
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &TanksCalculation::update);
    timer->start(1000 / 60);
}

bool TanksCalculation::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        int keyType = key->key();
        if (keyType >=  Qt::Key_Left && keyType <= Qt::Key_Down)
        {
            direction = (Direction)keyType;
            return false;
        }

        if (keyType == Qt::Key_Space)
        {
            QMetaObject::invokeMethod(mainTank, "createBullet");
            bullets = mapItem->findChildren<QQuickItem*>("bullet");
        }


    }
    else
        if (event->type() == QEvent::KeyRelease)
        {
            QKeyEvent* key = static_cast<QKeyEvent*>(event);
            if (key->key() == direction)
            {
                direction = None;
            }
        }


    return false;
}

void TanksCalculation::update() //TODO: make calculation in seperate thread
{
    updateTankPath(direction, mainTank);
    updateBulletPath();
    enemyMove();
}

void TanksCalculation::enemyMove() //TODO: piece of shit, need to rewrite.
{
    for (int i = 0; i < enemyTanks.count(); ++i)
    {
        QQuickItem *tank = enemyTanks[i].getTank();
        int tankDirection = int(tank->rotation() / 90 + 1) % 4 + Left;
        bool moved = updateTankPath(static_cast<Direction>(tankDirection), tank);
        Direction newDirections[3];

        unsigned int directionCount = 0;
        map.getPosibleNewDirectionToItem(tank, static_cast<Direction>(tankDirection), newDirections, directionCount);

        if (!moved)
        {
            switch (tankDirection) {
            case Up:
                newDirections[directionCount] = Down;
                break;
            case Down:
                newDirections[directionCount] = Up;
                break;
            case Left:
                newDirections[directionCount] = Right;
                break;
            case Right:
                newDirections[directionCount] = Left;
                break;
            default:
                break;
            }
            ++directionCount;
            tankDirection = newDirections[rand()%directionCount];
            updateTankPath(static_cast<Direction>(tankDirection), tank);
        }

    }
}





bool TanksCalculation::updateTankPath(Direction drc, QQuickItem *tank)
{
    qreal x = tank->x();
    qreal y = tank->y();

    switch (drc) {
    case Up:
        y -= SPEED;
        tank->setRotation(0);
        break;
    case Down:
        y += SPEED;
        tank->setRotation(180);
        break;
    case Left:
        x -= SPEED;
        tank->setRotation(270);
        break;
    case Right:
        x += SPEED;
        tank->setRotation(90);
        break;
    default:
        break;
    }


    if ( !(map.checkCollisionForrect(x, y, tank->width(), tank->height())) )
        return false;

    QMetaObject::invokeMethod(tank, "setCoordinates", Q_ARG(QVariant, (x)), Q_ARG(QVariant, (y)));

    return true;

}



void TanksCalculation::updateBulletPath()
{

    for (int i = 0; i < bullets.count(); ++i)// TODO: make earse remove
    {
        QMetaObject::invokeMethod(bullets[i], "next");
        if (bullets[i]->x() < 0 || bullets[i]->y()< 0 || bullets[i]->x() + bullets[i]->width() > mapItem->width()
                || bullets[i]->y() + bullets[i]->height() > mapItem->height())
        {
            QQuickItem *item = bullets[i];
            bullets.removeAt(i);
            QMetaObject::invokeMethod(item, "deleteMe");
            continue;
        }

        Zone **zonesOfItem = new Zone*[4];
        int zonesCount = 0;
        map.findZonesOfItem(bullets[i]->x(), bullets[i]->y(), bullets[i]->width(), bullets[i]->height(), zonesOfItem, &zonesCount);

        for (int j = 0; j < zonesCount; ++j)
        {
            if (zonesOfItem[j]->removeBricksAtPath(bullets[i]->x(), bullets[i]->y(), bullets[i]->width(), bullets[i]->height()))
            {
                QQuickItem *item = bullets[i];
                bullets.removeAt(i);
                QMetaObject::invokeMethod(item, "deleteMe");
                break;
            }
        }
        delete [] zonesOfItem;

        //        for (int j = 0; j < bricks.count(); ++j)
        //        {
        //            if (checkCollisionsOfItems(bullets[i]->position(), bricks[j]->position(), bullets[i]->width(), bricks[j]->width(),
        //                                       bullets[i]->height(), bricks[j]->height()))
        //            {

        //                QQuickItem *item = bullets[i];
        //                bullets.removeAt(i);
        //                QMetaObject::invokeMethod(item, "deleteMe");

        //                item = bricks[j];
        //                bricks.removeAt(j);
        //                QMetaObject::invokeMethod(item, "deleteMe");
        //                break;
        //            }

        //        }
    }
}
