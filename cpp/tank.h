#ifndef TANK_H
#define TANK_H

#include <QQuickItem>

class Tank
{
public:
    Tank(QQuickItem*item);
    void setCouldChangeDirection(bool ccd);
    bool getPreviousDirection();
    QQuickItem* getTank();
protected:
    QQuickItem *tankItem;
    void remove();
    bool couldChangeDirection;
};

#endif // TANK_H
