#ifndef MAP_H
#define MAP_H
#include <QQuickItem>

#include "zone.h"

enum Direction
{
    None = 0,
    Left = Qt::Key_Left,
    Up = Qt::Key_Up,
    Right = Qt::Key_Right,
    Down = Qt::Key_Down
} ;

class Map
{
public:
    Map();
    void setMapItem(QQuickItem*);
    bool checkCollisionForrect(qreal x, qreal y, qreal width, qreal height);
    void findZonesOfItem(qreal x, qreal y, qreal width, qreal height, Zone **zonesOfItem, int *zonesCount); // should be array on 4 pointers
    void appendZone(Zone z);
    void getPosibleNewDirectionToItem(QQuickItem *item, Direction currentDirection, Direction *buffer, unsigned int &directionsCount);
protected:
    QQuickItem *mapItem;
    QList <Zone> zones;

};

#endif // MAP_H
