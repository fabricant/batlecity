#include "zone.h"

Zone::Zone(QQuickItem *item):zoneItem(item) {}

QQuickItem *Zone::getZone()
{
    return zoneItem;
}

QList<QQuickItem *> Zone::getBricks()
{
    return zoneItem->findChildren<QQuickItem*>("brick");

}

bool Zone::checkColisitionWithRect(qreal x, qreal y, qreal width, qreal height)
{
    QList<QQuickItem*> bricks = getBricks();
    for (int i = 0; i < bricks.count(); ++i)
    {
        if (checkCollisionsOfItems(x, bricks[i]->x() + zoneItem->x(), y,
                                   bricks[i]->y() + zoneItem->y(), width, bricks[i]->width(), height, bricks[i]->height()))
            return true;
    }
    return false;
}

bool Zone::removeBricksAtPath(qreal x, qreal y, qreal width, qreal height)
{
    QList<QQuickItem*> bricks = getBricks();
    bool returnValue = false;

    QList<QQuickItem*>::iterator iterator = bricks.begin();

    while (iterator != bricks.end())
    {
        if (checkCollisionsOfItems(x, (*iterator)->x() + zoneItem->x(), y,
                                   (*iterator)->y() + zoneItem->y(), width, (*iterator)->width(), height, (*iterator)->height()))
        {
            QQuickItem* item = *iterator;
            QMetaObject::invokeMethod(item, "deleteMe");
            returnValue = true;
        }
             ++iterator;

    }
    return returnValue;
}



bool Zone::checkCollisionsOfItems(qreal x1, qreal x2, qreal y1, qreal y2, qreal width1, qreal width2,
                                  qreal height1, qreal height2)
{
    if (((x1 > x2 && x1 < x2 + width2) // x1 inside rect2

         || (x2 > x1 && x2 < x1 + width1)) // x2 inside rect1

            && ((y1 > y2 && y1 < y2 + height2) // y1 inside rect2

                || (y2 > y1 && y2 < y1 + height1))) // y2 inside rect1

        return true;

    //    qDebug() << "(x1 > x2 && x1 < x2 + width2): " <<  (x1 > x2 && x1 < x2 + width2);
    //    qDebug() << "(x2 > x1 && x2 < x1 + width1)" << (x2 > x1 && x2 < x1 + width1);
    //    qDebug() << "(y1 > y2 && y1 < y2 + height2)" << (y1 > y2 && y1 < y2 + height2);
    //    qDebug() << "(y2 > y1 && y2 < y1 + height1)" << (y2 > y1 && y2 < y1 + height1);
    return false;
}
